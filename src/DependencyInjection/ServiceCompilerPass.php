<?php
/**
 * @author Muryumin Artem <muryumin15@gmail.com>
 */


namespace App\DependencyInjection;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ServiceCompilerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container): void
    {
        $services = $container->findTaggedServiceIds('dto');

        foreach ($services as $id => $tags) {
            $definition = $container->findDefinition($id);
            $definition->addMethodCall('register', [new Reference(\JMS\Serializer\SerializerInterface::class)]);
        }
    }
}