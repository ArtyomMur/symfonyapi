<?php

namespace App\DataFixtures;

use App\Entity\Consumer;
use App\Entity\Provider;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /** @var array  */
    protected $entities = [
        Consumer::class => 10,
        Provider::class => 10,
        User::class => 1,
        'ProviderConsumer' => 3,
    ];
    /** @var array  */
    protected $uniqueAssocIndexes = [];
    /** @var UserPasswordEncoderInterface */
    protected $encoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->encoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->entities as $entityClass => $objCount) {
            $entityArr = explode('\\', $entityClass);
            $className = $entityArr[array_key_last($entityArr)];
            /**
             * @see AppFixtures::getUser()
             * @see AppFixtures::getProviderConsumer()
             */
            $objectBuilder = 'get' . ucfirst($className);
            $needCustomLoad = method_exists($this, $objectBuilder);
            $i = 1;
            while ($objCount >= $i) {
                if ($needCustomLoad) {
                    $entityObject = $this->$objectBuilder($i);
                } else {
                    /** @var Provider|Consumer $entityObject */
                    $entityObject = new $entityClass;
                    $entityObject->setName($entityClass . "_#{$i}");
                }
                $manager->persist($entityObject);
                $i++;
            }
        }

        $manager->flush();
    }

    /**
     *
     * @param $i
     *
     * @return User
     */
    private function getUser($i)
    {
        $user = new User();
        $user->setUsername("User_#{$i}");
        $user->setPassword($this->encoder->encodePassword($user, "Password{$i}"));

        return $user;
    }

    /**
     * @return Provider
     */
    private function getProviderConsumer()
    {
        static $maxProviderId, $maxConsumerId;

        if (!$maxProviderId && !$maxConsumerId) {
            $maxProviderId = $this->entities[Provider::class] - 1;
            $maxConsumerId = $this->entities[Consumer::class] - 1;
        }

        $parentId = mt_rand(1, $maxProviderId);
        $provider = (new Provider())->setId($parentId)->setName(Provider::class . "_#{$parentId}");
        $tries = 3;
        while ($tries-- > 0) {
            $id = mt_rand(1, $maxConsumerId);
            $consumer = (new Consumer())->setId($id)->setName(Consumer::class . "_#{$id}");
            $provider->addConsumer($consumer);
        }

        return $provider;
    }
}
