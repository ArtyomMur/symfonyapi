<?php
/**
 * @author Muryumin Artem <muryumin15@gmail.com>
 */


namespace App\Service;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{

    /**
     * @param GetResponseForExceptionEvent $event
     *
     * @throws \Throwable
     */
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();

        $event->setResponse(new JsonResponse([
            'message' => $exception->getMessage(),
            'class' => get_class($exception)
            ], 500));
        $event->allowCustomResponseCode();
    }
}