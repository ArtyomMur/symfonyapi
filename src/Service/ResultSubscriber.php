<?php
/**
 * @author Muryumin Artem <muryumin15@gmail.com>
 */


namespace App\Service;


use App\DTO\DtoInterface;
use JMS\Serializer\SerializationContext;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class ResultSubscriber
{

    /**
     * @var \JMS\Serializer\SerializerInterface
     */
    protected $serializer;
    /**
     * @var \Symfony\Component\Security\Core\Security
     */
    protected $manager;

    public function __construct(\JMS\Serializer\SerializerInterface $serializer, \Symfony\Component\Security\Core\Security $manager)
    {
        $this->serializer = $serializer;
        $this->manager = $manager;
    }
    /**
     * @param GetResponseForControllerResultEvent $event
     * @throws InsuranceExceptionInterface
     */
    public function onKernelResponse(GetResponseForControllerResultEvent $event): void
    {
        //TODO publicDTO поробовать разделить интерфесы и проверять
        /** @var DtoInterface $result */
        $result = $event->getControllerResult();
        if (!is_a($result, DtoInterface::class)) {
            return;
        }

        $isAuth = true;
        $serializerContext = new SerializationContext();
        $serializerContext->setGroups(array_merge(['public'], $isAuth ? ['private'] : []));
        $result->register($this->serializer, $serializerContext);
        $event->setResponse(new JsonResponse($result->getData(), $result->getCode()));
    }
}