<?php
/**
 * @author Muryumin Artem <muryumin15@gmail.com>
 */


namespace App\Service;


class SerializerService
{
    /** @var \JMS\Serializer\SerializerInterface  */
    private $serializer;


    public function __construct()
    {
        $this->serializer = \JMS\Serializer\SerializerBuilder::create()->build();
    }

    /**
     * @param mixed  $data
     * @param string $format
     *
     * @return string
     */
    public function serialize($data, string $format = 'json'): string
    {
        return $this->serializer->serialize($data, $format);
    }

    /**
     * @param string $data
     * @param string $type
     * @param string $format
     *
     * @return mixed
     */
    public function deserialize(string $data, string $type = 'array', string $format = 'json')
    {
        return $this->serializer->deserialize($data, $type, $format);
    }
}