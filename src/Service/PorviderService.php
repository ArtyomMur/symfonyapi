<?php
/**
 * @author Muryumin Artem <muryumin15@gmail.com>
 */


namespace App\Service;


use App\DTO\Deleted;
use App\DTO\DtoInterface;
use App\Entity\Provider;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PorviderService
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function get(int $id)
    {
        $provider = $this->find($id);

        return new \App\DTO\Provider($provider);
    }
    public function delete(int $id): DtoInterface
    {

        $provider = $this->find($id);
        $this->em->remove($provider);
        $this->em->flush();

        return new Deleted();
    }

    /**
     * @param int $id
     *
     * @return object|null
     */
    private function find(int $id)
    {
        $provider = $this->em->getRepository(Provider::class)->find($id);
        if (!$provider) {
            throw new \LogicException('Provider not found');
        }
        return $provider;
}
}