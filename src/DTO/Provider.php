<?php
/**
 * @author Muryumin Artem <muryumin15@gmail.com>
 */


namespace App\DTO;


use App\Entity\Provider as Data;
use JMS\Serializer\SerializationContext;

class Provider implements DtoInterface
{

    /**
     * @var Data
     */
    protected $provider;

    /** @var \JMS\Serializer\SerializerInterface */
    private $serializer;
    /** @var SerializationContext */
    private $context;

    public function __construct(Data $provider)
    {

        $this->provider = $provider;
    }

    public function getData(): array
    {
        return [
            'status' => 'ok',
            'data' => $this->serializer->toArray($this->provider, $this->context)
        ];
    }

    public function getCode(): int
    {
       return 200;
    }

    public function register(\JMS\Serializer\SerializerInterface $serializer, SerializationContext $context): void
    {
        $this->serializer = $serializer;
        $this->context = $context;
    }
}