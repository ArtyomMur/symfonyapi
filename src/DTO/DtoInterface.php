<?php
/**
 * Created by PhpStorm.
 * User: Muryumin Artem
 * Date: 07.09.2019
 * Time: 13:53
 */

namespace App\DTO;


use JMS\Serializer\SerializationContext;

interface DtoInterface
{

    public function getData(): array;

    public function getCode(): int;

    public function register(\JMS\Serializer\SerializerInterface $serializer, SerializationContext $context): void;
}