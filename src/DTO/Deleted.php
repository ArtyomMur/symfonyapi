<?php
/**
 * @author Muryumin Artem <muryumin15@gmail.com>
 */


namespace App\DTO;


use JMS\Serializer\SerializationContext;

class Deleted implements DtoInterface
{

    public function getData(): array
    {
        return [
            'status' => 'ok'
        ];
    }

    public function getCode(): int
    {
       return 200;
    }

    public function register(\JMS\Serializer\SerializerInterface $serializer, SerializationContext $context): void
    {
        // TODO: Implement register() method.
    }
}