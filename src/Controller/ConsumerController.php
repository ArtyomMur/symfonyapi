<?php

namespace App\Controller;

use App\Entity\Consumer;
use App\Repository\ConsumerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/consumer")
 */
class ConsumerController extends BaseController
{
    /**
     * @Route("/", name="consumer_index", methods={"GET"})
     */
    public function index(ConsumerRepository $consumerRepository): Response
    {
        return $this->getSerializedResponse($consumerRepository->findAll());
    }

    /**
     * @Route("/new", name="consumer_new", methods={"POST"})
     */
    public function new(Request $request): Response
    {
        $requestData = $this->deserialize($request->getContent());
        $consumer = (new Consumer())->buildObject($requestData);

        if (empty($consumer->getName())) {
            return $this->getSerializedResponse(self::FIELD_MISSING_ERROR, self::RESPONSE_TYPE_ERROR);
        }

        $entityManager = $this->getEntityManager();
        $entityManager->persist($consumer);
        $entityManager->flush();
        /** @var ConsumerRepository $repo */
        $repo = $this->getRepository();
        $insertedId = $repo->getLastInsertId();

        return $this->getSerializedResponse($insertedId);
    }

    /**
     * @Route("/show/{id}", name="consumer_show", methods={"GET"})
     */
    public function show(Consumer $consumer): Response
    {
        return $this->getSerializedResponse($consumer);
    }

    /**
     * @Route("/{id}/edit", name="consumer_edit", methods={"POST"})
     */
    public function edit(Request $request, $id): Response
    {
        $consumer = $this->getEntityManager()->find(Consumer::class, $id);
        if (!$consumer) {
            return $this->getSerializedResponse(self::NOT_FOUND_ERROR, self::RESPONSE_TYPE_ERROR);
        }
        $requestData = $this->deserialize($request->getContent());
        $consumer = $consumer->buildObject($requestData);
        $this->getEntityManager()->flush();

        return $this->getSerializedResponse($consumer);
    }

    /**
     * @Route("/{id}", name="consumer_delete", methods={"DELETE"})
     */
    public function delete(Request $request, $id): Response
    {
        $consumer = $this->getEntityManager()->find(Consumer::class, $id);
        if (!$consumer) {
            return $this->getSerializedResponse(self::NOT_FOUND_ERROR, self::RESPONSE_TYPE_ERROR);
        }
        $this->getEntityManager()->remove($consumer);
        $this->getEntityManager()->flush();

        return $this->getSerializedResponse(['removed' => 'true']);
    }
}
