<?php

namespace App\Controller;

use App\DTO\DtoInterface;
use App\Entity\Provider;
use App\Repository\ProviderRepository;
use App\Service\PorviderService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/provider")
 */
class ProviderController extends BaseController
{
    /**
     * @Route("/new", name="provider_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $requestData = $this->deserialize($request->getContent());
        $provider = (new Provider())->buildObject($requestData);

        if (empty($provider->getName())) {
            return $this->getSerializedResponse(self::FIELD_MISSING_ERROR, self::RESPONSE_TYPE_ERROR);
        }

        $entityManager = $this->getEntityManager();
        $entityManager->persist($provider);
        $entityManager->flush();
        /** @var ProviderRepository $repo */
        $repo = $this->getRepository();
        $insertedId = $repo->getLastInsertId();

        return $this->getSerializedResponse($insertedId);
    }

    /**
     * @Route("/show/{id}", name="provider_show", methods={"GET"})
     */
    public function show(PorviderService $service, $id): DtoInterface
    {
        return $service->get($id);
    }

    /**
     * @Route("/{id}/edit", name="provider_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, $id): Response
    {
        $provider = $this->getEntityManager()->find(Provider::class, $id);
        if (!$provider) {
            return $this->getSerializedResponse(self::NOT_FOUND_ERROR, self::RESPONSE_TYPE_ERROR);
        }
        $requestData = $this->deserialize($request->getContent());
        $provider = $provider->buildObject($requestData);
        $this->getEntityManager()->flush();

        return $this->getSerializedResponse($provider);
    }

    /**
     * @Route("/{id}", name="provider_delete", methods={"DELETE"})
     */
    public function delete(PorviderService $service, $id): DtoInterface
    {
        return $service->delete($id);
    }

    /**
     * @Route("/list/{offset}/{limit}", defaults={"offset" = 0, "limit"=10 }, name="provider_list")
     */
    public function getList($offset, $limit): Response
    {
        /** @var ProviderRepository $repo */
        $repo = $this->getRepository();
        if (!$this->getUser()) {
            return $this->getSerializedResponse($repo->getOnlyProvider());
        }
        $providersWithConsumers = $repo->findBy([], ['id'=> 'DESC'], $limit, $offset);

        return $this->getSerializedResponse($providersWithConsumers);
    }
}
