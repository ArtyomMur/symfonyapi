<?php
/**
 * @author Muryumin Artem <muryumin15@gmail.com>
 */


namespace App\Controller;


use App\Service\SerializerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    /**
     * Response types
     * @var string
     */
    const
        RESPONSE_TYPE_SUCCESS = 'success',
        RESPONSE_TYPE_ERROR = 'error';
    /**
     * Errors
     * @var string
     */
    const
        NOT_FOUND_ERROR = 'Not found',
        FIELD_MISSING_ERROR = 'Required field name is empty';


    /** @var SerializerService */
    protected $serializer;

    /**
     * BaseController constructor.
     *
     * @param SerializerService $serializer
     */
    public function __construct(SerializerService $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param  mixed $data
     * @param string $format
     *
     * @return string
     */
    public function serialize($data, $format = 'json'): string
    {
        return $this->serializer->serialize($data, $format);
    }

    /**
     * @param string $data
     * @param string $type
     * @param string $format
     *
     * @return mixed
     */
    public function deserialize(string $data, string $type = 'array', string $format = 'json')
    {
        return $this->serializer->deserialize($data, $type, $format);
    }

    /**
     * @param mixed  $data
     * @param string $type
     *
     * @return Response
     */
    public function getSerializedResponse($data, $type = self::RESPONSE_TYPE_SUCCESS)
    {
        $response = [
            'status' => $type,
            'data' => $data
        ];
        $serializedContent = $this->serialize($response);

        return new Response($serializedContent);
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param string|null $class
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository($class = null)
    {
        if (!$class) {
            $controllerFullNameClass = explode('\\', get_class($this));
            $controllerClass = $controllerFullNameClass[array_key_last($controllerFullNameClass)];
            $class = str_replace('Controller', '', $controllerClass);
        }

        return $this->getEntityManager()->getRepository('App\Entity\\' . $class);
    }
}