<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Consumer;
use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\ExclusionPolicy("all")
 * @JMS\VirtualProperty(
 *     "consumers",
 *     exp="object.getId() > 24 ? object.getConsumers().toArray() : null",
 *     options={
 *        @JMS\SerializedName("consumers"),
 *        @JMS\Expose(if="object.getConsumers()"),
 *        @JMS\Groups({"private"}),
 *        @JMS\Type("array")
 *     }
 *  )
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProviderRepository")
 */
class Provider
{
    /**
     * @JMS\Expose
     * @JMS\Groups({"private"})
     * @JMS\Type("integer")
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @JMS\Expose
     * @JMS\Groups({"public", "private"})
     * @JMS\Type("string")
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    /**
     * @ORM\ManyToMany(targetEntity="Consumer", inversedBy="providers", cascade={"persist"}, fetch="EXTRA_LAZY")
     *
     * @var ArrayCollection
     */
    private $consumers;


    public function __construct()
    {
        $this->consumers = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getConsumers()
    {
        return $this->consumers;
    }

    /**
     * @param \App\Entity\Consumer $consumer
     */
    public function addConsumer(Consumer $consumer)
    {
        if (!$this->consumers->contains($consumer)) {
            $this->consumers[] = $consumer;
        }
    }
    /**
     * @param mixed $id
     *
     * @return Provider
     */
    public function setId($id): Provider
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param ArrayCollection $consumers
     */
    public function setConsumers(ArrayCollection $consumers): void
    {
        $this->consumers = $consumers;
    }

    public function buildObject(array $data)
    {
        foreach ($data as $field => $value) {
            $methodName = 'set' . ucfirst($field);
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            }
        }

        return $this;
    }
}
