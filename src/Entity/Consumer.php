<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Provider;
use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass="App\Repository\ConsumerRepository")
 */
class Consumer
{
    /**
     * @JMS\Expose
     * @JMS\Groups({"private"})
     * @JMS\Type("integer")
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @JMS\Expose
     * @JMS\Groups({"private"})
     * @JMS\Type("string")
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="Provider", mappedBy="consumers", cascade={"persist"})
     * @ORM\JoinTable(name="provider_consumer")
     *
     * @var Collection
     */
    private $providers;

    public function __construct()
    {
        $this->providers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @param mixed $id
     *
     * @return Consumer
     */
    public function setId($id): Consumer
    {
        $this->id = $id;
        return $this;
    }

    public function buildObject(array $data)
    {
        foreach ($data as $field => $value) {
            $methodName = 'set' . ucfirst($field);
            if (method_exists($this, $methodName)) {
                call_user_func_array([$this, $methodName], $value);
            }
        }

        return $this;
    }
}
