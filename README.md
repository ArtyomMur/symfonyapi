## TASK
### Реализовать REST API на фреймворке Symfony

Реализовать REST API на фреймворке Symfony
- для авторизации использовать JWT токен
- для базы использовать sqlitе, использовать миграции и фикстуры
- для генерации ответа использовать JMS Serializer
- дожны бить реализованы 2 сущности
    - родительская сущность
    - дочерняя сущность
    - между ними связь многие ко многим
- CRUD нужно реализовать для обоих сущностей
   - для родительской сущности доступны всем
   - для дочерней сущности доступны только авторизованным
- реализовать метод получения нескольких записей основной сущности
   - должна быть реализована пагинация
   - для авторизованных должен отдаваться родительская и дочерние сущности
   - для не авторизованных должны отдаваться родительская без дочерних сущностей
   
 - добавление рассчетного поля (цена) дл сущности провайдер
 - добавить тип калькуляции этого поля
 - реализовать сервис расчета калькуляции, который будет использоать список сервисов калькуляторов, которые подгружаются через теги
   - для авторизованных пользователей применять сервис калькуляции скидки
   
   * использовать для рассчета цены данные из связанной сущности
   * вместо явного получения сущности по id передавать в контроллер ДТО полученный из листенера/сабскрайбера 
      
---

### Setup
composer install --platform-ignore-reqs

php bin/console doctrine:migrations:migrate

php bin/console doctrine:fixtures:load


#### get jwt token
php bin/console server:run

curl -X POST -H "Content-type: application/json" localhost:8000/api/login_check -d '{"username": "User_#1", "password": "Password1"}' 

"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1Njc4NTcxMDMsImV4cCI6MTU2Nzg2MDcwMywicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiVXNlcl8jMSJ9.SFrRX_51YT__nRtpzJF6Uu7VuATMqHBsFxgEhc0cw9C5Di-mDtJVCv-zi-U_TFm3crcYA1F4e81obS3PKml0UQ9UK9sk84SUjN8mAy9cw00y21TVkts9_uJIYA6vIT2UA9kEk0_i7A-S0BJA0rxzZGtiHCoWDDeXXH99Zs0UrrJTv4AKgPIv5t6SzneWbmJfnqoynAzAKw0ESwo8-2IyZ62mcIaEI2cbDJNB-Rnc7TItYtScqyIZsGGOFtFoO_q34trxLBtT4-yhVdV-Wyv3qJJADpJm6E9eKQ5FdM7EDWzph7ZubV4tqmNJnZEroKSnEfziImO-fsqUTII2YHHVmj7cMynIxoy531CAZ5ey8r8UUYpf7xss8al0sM-9gapUM6hvbW2tEjV4_e0_NjP5cck28YYmLfOr4L6RWIiduU9nn9IUNTnLo7OVQvfzobm--1cTu4BJzOjJlHAKFu8XXpTLVakM9DSb5ih3cQOZ6g0ScYjW3mqXYPEpEOElZvMuhQenu-9q0Q1DYTSLk-4Rzaf3GQEKvzxVbvjnksFjUKP_uPrmrCa5_5n5_j-dRGrcBFLjQdpSCTygsvewVhbck2kg6VEkil7Bq4BontCtIZFOCSTeNmsfpsWR-fFP7-_-uTDyZsxgC999nQbt1mfpMMIgaQ-4SgkZ5yLXeVuAGz4"

  consumer_index             GET        ANY      ANY    /api/consumer/                       
  consumer_new               POST       ANY      ANY    /api/consumer/new                    
  consumer_show              GET        ANY      ANY    /api/consumer/show/{id}              
  consumer_edit              POST       ANY      ANY    /api/consumer/{id}/edit              
  consumer_delete            DELETE     ANY      ANY    /api/consumer/{id}                   
  provider_new               GET|POST   ANY      ANY    /api/provider/new                    
curl -X GET -H "Authorize: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1Njc4NTcxMDMsImV4cCI6MTU2Nzg2MDcwMywicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiVXNlcl8jMSJ9.SFrRX_51YT__nRtpzJF6Uu7VuATMqHBsFxgEhc0cw9C5Di-mDtJVCv-zi-U_TFm3crcYA1F4e81obS3PKml0UQ9UK9sk84SUjN8mAy9cw00y21TVkts9_uJIYA6vIT2UA9kEk0_i7A-S0BJA0rxzZGtiHCoWDDeXXH99Zs0UrrJTv4AKgPIv5t6SzneWbmJfnqoynAzAKw0ESwo8-2IyZ62mcIaEI2cbDJNB-Rnc7TItYtScqyIZsGGOFtFoO_q34trxLBtT4-yhVdV-Wyv3qJJADpJm6E9eKQ5FdM7EDWzph7ZubV4tqmNJnZEroKSnEfziImO-fsqUTII2YHHVmj7cMynIxoy531CAZ5ey8r8UUYpf7xss8al0sM-9gapUM6hvbW2tEjV4_e0_NjP5cck28YYmLfOr4L6RWIiduU9nn9IUNTnLo7OVQvfzobm--1cTu4BJzOjJlHAKFu8XXpTLVakM9DSb5ih3cQOZ6g0ScYjW3mqXYPEpEOElZvMuhQenu-9q0Q1DYTSLk-4Rzaf3GQEKvzxVbvjnksFjUKP_uPrmrCa5_5n5_j-dRGrcBFLjQdpSCTygsvewVhbck2kg6VEkil7Bq4BontCtIZFOCSTeNmsfpsWR-fFP7-_-uTDyZsxgC999nQbt1mfpMMIgaQ-4SgkZ5yLXeVuAGz4" localhost:8000/api/provider/show/{id}
  provider_edit              GET|POST   ANY      ANY    /api/provider/{id}/edit              
  curl -X DELETE -H "Content-type: application/json" localhost:8000/api/provider/{id}                  
  provider_list              ANY        ANY      ANY    /api/provider/list/{offset}/{limit}  
  api_login_check            ANY        ANY      ANY    /api/login_check        